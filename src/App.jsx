import { BrowserRouter as Router, Switch } from 'react-router-dom'
import routes, { makeRoutes, makeNavLinks } from './routes'

const App = () => {
  return (
    <Router>
      <div className="container d-flex justify-content-between mt-3">
        <div style={{ width: '200px' }}>
          {makeNavLinks(routes)}
        </div>
        <div style={{ flex: 1 }}>
          <Switch>
            {makeRoutes(routes)}
          </Switch>
        </div>
      </div>
    </Router>
  )
}

export default App
