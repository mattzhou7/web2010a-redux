import React, { useCallback, useMemo, useState } from "react";

function facOf(n) {
  console.log('facOf 运行', 'n =', n)
  return n <= 0 ? 1 : n*facOf(n-1)
}


const ChildComponent = React.memo(() => {
  console.log("ChildComponent render")
  return <div>ChildComponent</div>
})

const MemoVsCallback = () => {
  const [number, setNumber] = useState(1)
  const [inc, setInc] = useState(0)

  const fac = useMemo(() => {
    return facOf(number)
  }, [number])

  const handleClick = () => setInc(inc+1)

  const childCallback = useCallback((text) => {
    console.log(text)
  }, [])

  return ( <div>
    <input size={5} type='text' defaultValue={number} onChange={(e) => {
      setNumber(parseInt(e.target.value) || 0)
    }} />
    的阶乘结果 = {fac}
    <button onClick={handleClick}>重新渲染</button>
    <ChildComponent callback={childCallback}/>
  </div> );
}

export default MemoVsCallback;