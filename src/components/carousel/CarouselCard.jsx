import React from "react";

const CarouselCard = ({domTag='li', children, ...props}) => {
  return React.createElement(
    domTag,
    props,
    children
  )
  // return ( <li {...props}>{children}</li> );
}

export default CarouselCard;