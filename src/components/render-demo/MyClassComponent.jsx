import React, { Component } from 'react';
import MyFunctionComponent from './MyFunctionComponent';
import MyFunctionComponent2 from './MyFunctionComponent2';

class MyClassComponent extends Component {
  state = {
    count: 0
  }
  render() {
    console.log(this.props.name, 'class组件渲染', Date.now())
    return ( <div>
      {this.props.name} :
      <button
        onClick={() =>
          this.setState({count: this.state.count+1})
        }
      >add</button> {this.state.count}
      <MyFunctionComponent2 name="c3"/>
    </div> );
  }
}

export default MyClassComponent;