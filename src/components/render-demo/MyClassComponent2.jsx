import React, { PureComponent } from 'react';
import MyFunctionComponent from './MyFunctionComponent';

class MyClassComponent2 extends PureComponent {
  state = {
    count: 0
  }
  render() {
    console.log(this.props.name, 'class组件渲染', Date.now())
    return ( <div>
      {this.props.name} :
      <button
        onClick={() =>
          this.setState({count: this.state.count+1})
        }
      >add</button> {this.state.count}
      <MyFunctionComponent name="c5"/>
    </div> );
  }
}

export default MyClassComponent2;