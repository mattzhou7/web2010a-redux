import { useState } from "react";

const MyFunctionComponent = ({name}) => {
  console.log(name, "function组件渲染", Date.now())
  const [count, setCount] = useState(0)
  return ( <div>
    {name}:
    <button onClick={() => setCount(count+1)}>
      add
    </button>
    {count}
  </div> );
}

export default MyFunctionComponent;