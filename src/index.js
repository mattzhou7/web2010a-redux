import ReactDOM from 'react-dom'
import App from './App'
import 'bootstrap/dist/css/bootstrap.css'
// import '@forevolve/bootstrap-dark/dist/css/bootstrap-dark.css'

ReactDOM.render(
  <App />,
  document.getElementById('root')
)