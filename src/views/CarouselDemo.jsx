import Carousel1 from '../components/carousel/Carousel1'
import Carsousel2 from '../components/carousel/Carousel2'
import CarouselCard from '../components/carousel/CarouselCard'
import '../components/carousel/style.css'

const CarouselDemo = () => {
  return (
    <>
      <Carousel1>
        <CarouselCard className="red big-text">1</CarouselCard>
        <CarouselCard className="blue big-text">2</CarouselCard>
        <CarouselCard className="green big-text">3</CarouselCard>
      </Carousel1>
      <Carsousel2>
        <CarouselCard domTag="div" className="red big-text">
          1
        </CarouselCard>
        <CarouselCard domTag="div" className="blue big-text">
          2
        </CarouselCard>
        <CarouselCard domTag="div" className="green big-text">
          3
        </CarouselCard>
      </Carsousel2>
    </>
  )
}

export default CarouselDemo
