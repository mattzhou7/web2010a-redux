import { useEffect, useMemo, useState } from 'react'
import MemoVsCallback from '../components/MemoVsCallback'



const MemoAndCallbackDemo = () => {
  console.log('render')
  const [count, setCount] = useState(0)
  useEffect(() => {
    // console.log('onMount/onUpdate')
    return () => {
      // console.log('onUnmount')
    }
  })

  const computeWordCount = (words) => {
    let i = 0
    while (i < 10 ** 9) i++
    return words.length
  }
  const [words, setWords] = useState(['hello', 'world', '你好'])
  const wordCount = useMemo(() => {
    console.log('memo run')
    return computeWordCount(words)
  }, [words, count])

  return (
    <>
      词语个数：{wordCount}
      <button onClick={() => setCount(count + 1)}>add: {count}</button>
      <button onClick={() => setCount(count - 1)}>sub: {count}</button>
      <hr />
      <MemoVsCallback />
    </>
  )
}

export default MemoAndCallbackDemo
