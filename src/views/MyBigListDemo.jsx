import React, { useCallback, useState } from 'react'

const bigArray = [...Array(2000)].map((_, index) => index)
// console.log(bigArray)

const List = React.memo(({ items, onItemClick }) => {
  return (
    <ul>
      {items.map((item) => (
        <Item key={item} text={item} onClick={onItemClick} />
      ))}
    </ul>
  )
})

const Item = ({ text, onClick }) => {
  console.log('Item', text)
  return <li onClick={onClick}>{text}</li>
}

const MyBigListDemo = () => {
  const start = Date.now()
  const [count, setCount] = useState(0)
  const [search, setSearch] = useState('')
  const handleItemClick = useCallback((e) => {
    console.log(e.target.innerText, search)
  }, [search])
  const ret = (
    <>
      <h2>
        测试useCallback对大列表的影响
        <button onClick={() => setCount(count + 1)}>强制刷新</button>
        <button onClick={() => setSearch(Math.random())}>changeSearch</button>
      </h2>
      <List items={bigArray} onItemClick={handleItemClick}/>
    </>
  )
  const end = Date.now()
  console.log('渲染耗时(ms)：', end - start)
  return ret
}

export default MyBigListDemo
