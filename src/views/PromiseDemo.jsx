import { useEffect, useState } from "react";

const PromiseDemo = () => {
  const [user, setUser] = useState(null)
  useEffect(() => {
    const controller = new AbortController()
    fetch('https://randomuser.me/api/', {signal: controller.signal})
      .then(resp => resp.json())
      .then(result => setUser(result.results[0]))

    return () => controller.abort()
  }, [])
  return ( user && <h2>{user.name.first}</h2> );
}

export default PromiseDemo;