import { useState } from "react";
import MyClassComponent from "../components/render-demo/MyClassComponent";
import MyFunctionComponent from "../components/render-demo/MyFunctionComponent";
import MyClassComponent2 from '../components/render-demo/MyClassComponent2'
import MyFunctionComponent2 from "../components/render-demo/MyFunctionComponent2";
// dummy
const RenderDemo = () => {
  const [name, setName] = useState('c1')
  return ( <>
      <input
        type="text"
        value={name}
        onChange={(e) => {
          setName(e.target.value)
        }
      }/>
      <h2>class 组件</h2>
      <MyClassComponent name="c1"/>
      <h2>function 组件</h2>
      <MyFunctionComponent name="c2"/>
      <h2>pure class 组件</h2>
      <MyClassComponent2 name='c4' />
      <h2>memo 组件</h2>
      <MyFunctionComponent2 name='c6'/>
    </> );
}

export default RenderDemo;