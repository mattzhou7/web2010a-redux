import { useReducer } from "react"

/*
{
  todos: [{id, text, completed}],
  filter: 'all/active/completed'
}
*/
const reducer = (state, action) => {
  /*
    action.type
    {type: 'add', payload: {id, text}}
    {type: 'remove', payload: todoId}
    {type: 'toggle', payload: todoId}
    {type: 'filter', payload: 'all/active/completed'}
  */
  switch(action.type) {
    case 'add': {
      const todo = {
        id: action.payload.id ,
        text: action.payload.text,
        completed: false
      }
      return {...state, todos: [...state.todos, todo]}
    }
    case 'remove':
      return {...state, todos: state.todos.filter(
        todo => todo.id !== action.payload
      )}
    case 'toggle': {
      const todoIndex = state.todos.findIndex(todo =>
        todo.id === action.payload)
      const newTodo = {...state.todos[todoIndex], completed:
        !state.todos[todoIndex].completed
      }
      return {...state, todos: [
        ...state.todos.slice(0, todoIndex),
        newTodo,
        ...state.todos.slice(todoIndex+1)
      ]}
    }
    case 'filter':
      return {...state, filter: action.payload}
    default:
      return state
  }
}

const initState = {
  todos: [
    // {id: '1', text: 'abc', completed: false},
    // {id: '2', text: 'xyz', completed: true},
  ], filter: 'all'
}

const makeId = () => Math.random().toString(36).substr(-8)

// const s1 = reducer(initState, {type: 'add', payload: {
//   id: '1',
//   text: 'abc'
// }})
// const s2 = reducer(s1, {type: 'add', payload: {
//   id: '2',
//   text: 'xyz'
// }})
// console.log(s2)
// const s3 = reducer(s2, {type: 'remove', payload: '1'})
// console.log(s3)
// const s4 = reducer(s3, {type: 'toggle', payload: '2'})
// console.log(s4)

const TodoApp = () => {
  const [state, dispatch] = useReducer(reducer, initState)
  const visibleTodos = state.todos.filter(todo => {
    switch(state.filter) {
      case 'completed':
        return todo.completed
      case 'active':
        return !todo.completed
      case 'all':
      default:
        return todo
    }
  })
  return (
    <>
      <h2>待办事务清单</h2>
      <form onSubmit={e => {
        e.preventDefault()
        const text = e.target.text.value.trim()
        if (text.length>0) {
          dispatch({type:'add', payload: {id: makeId(), text}})
        }
        e.target.text.value = ''
        e.target.text.focus()
      }}><input name='text' type="text" /><button>添加</button></form>
      <ul>
        {
          visibleTodos.map(todo => (<li key={todo.id}>
              <span style={{ textDecoration:
                todo.completed ? 'line-through': 'none' }}
                onClick={() => {
                  dispatch({type: 'toggle', payload: todo.id})
                }}
              >
                {todo.text}
              </span>{' '}
              <button onClick={() => {
                dispatch({type: 'remove', payload: todo.id})
                }}>❌</button>
            </li>)
          )
        }
      </ul>
      <div>
        <a href='#all' onClick={e => {
          e.preventDefault()
          dispatch({type: 'filter', payload: 'all'})
        }} style={{
          color: state.filter === 'all' ? 'red':'blue'
        }}>全部</a>{' '}
        <a href='#active' onClick={e => {
          e.preventDefault()
          dispatch({type: 'filter', payload: 'active'})
        }} style={{
          color: state.filter === 'active' ? 'red':'blue'
        }}>未完成</a>{' '}
        <a href='#completed' onClick={e => {
          e.preventDefault()
          dispatch({type: 'filter', payload: 'completed'})
        }} style={{
          color: state.filter === 'completed' ? 'red':'blue'
        }}>已完成</a>
      </div>
    </>
  )
}

export default TodoApp
