import {
  UserContextProvider,
  useUserContextState,
} from '../components/UserApp/UserContext'

const UserApp = () => {
  return (
    <UserContextProvider>
      <Page />
    </UserContextProvider>
  )
}

const Page = () => {
  const user = useUserContextState()
  console.log(user)

  if (user?.login?.username) {
    return (
      <p>
        您已登录 [{user.login.username}]
        <img src={user.picture.thumbnail} alt="" />
      </p>
    )
  } else {
    return <p>您还没有登录</p>
  }
}

export default UserApp
