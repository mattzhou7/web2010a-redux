/*
state

{
  loading: bool,
  user: {username},
  error: '错误'
}

action
{type: 'login'} // 发起登录请求
{type: 'login_success'} // 请求成功，从服务器返回数据
{type: 'logout'} // 发起登出请求
{type: 'logout_success', payload: {user: {username: 'abc'}}} // 请求成功，从服务器返回数据

*/

import { useReducer } from 'react'

const users = [
  {username: 'zhangsan', password: '111111'},
  {username: 'lisi', password: '222222'},
]

const loginApi = (username, password) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const user = users.find(u => u.username === username)
      if (user) {
        if (user.password === password) {
          resolve(user)
        } else {
          reject('密码错误')
        }
      } else {
        reject('用户不存在')
      }
    }, Math.random() * 3000)
  })
}

const login = (form, dispatch) => {
  dispatch({type: 'login'})
  loginApi(form.username.value, form.password.value)
    .then((user) =>
      dispatch({type: 'login_success', payload: user})
    )
    .catch((error) => {
      form.password.value = ''
      dispatch({type: 'login_failure', payload: error})
    })
}

const logoutApi = () => new Promise((resolve) => {
  setTimeout(() => {
    resolve()
  }, 3000 * Math.random());
})

const logout = (dispatch) => {
  dispatch({type: 'logout'})
  logoutApi().then(() => dispatch({type: 'logout_success'}))
}

const reducer = (state, { type, payload }) => {
  console.log('action:', {type, payload})
  switch (type) {
    case 'logout':
    case 'login':
      return { ...state, loading: true, error: null }
    case 'login_success':
      return { ...state, loading: false, user: payload }
    case 'login_failure':
      return { ...state, loading: false, error: payload}
    case 'logout_success':
      return { ...state, loading: false, user: null }
    default:
      return state
  }
}
const initState = {
  loading: false,
  user: null,
  error: null,
}
const UserLogin = () => {
  const [{ loading, user, error }, dispatch] = useReducer(reducer, initState)
  console.log('state:',{loading, user, error})
  return (
    <>
      {user === null && (
        <form onSubmit={(e) => {
          e.preventDefault()
          login(e.target, dispatch)
        }}>
          <label>
            用户名：
            <input type="text" name="username" />
          </label>
          <br />
          <label>
            密码：
            <input type="password" name="password" />
          </label>
          <br />
          <button disabled={loading}>登录</button>
        </form>
      )}
      {error !== null && (
        <div className="alert alert-danger" role="alert">
          {error}
        </div>
      )}
      {user !== null && (
        <>
          <h2>{user.username}，欢迎回来</h2>
          <button  disabled={loading}
            onClick={() => logout(dispatch)}
          >登出</button>
        </>
      )}
      {loading && (
        <div className="spinner-border text-dark" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      )}
    </>
  )
}

export default UserLogin
